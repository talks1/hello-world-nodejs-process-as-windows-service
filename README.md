# Hello World Run Node Process as a windows service

We have a nodejs server process which needs to run on a Windows Server.


## PM2 Approach 

The idea is to use PM2.IO for this.

### Installing PM2 and pm2-windows-service with no network

The idea here is to install everything locally and then copy the local node_modules to remote host.

### Running setup

Found I couldnt successfully run 
```
pm2-service-install -n  production dashboard
```

### Blocked

This approach seems blocked

##  Node-Windows

Installed node-windows

Created a `install-service.js` and run this successfully.
Saw I could see a windows service there.

Created an `uninstall-service.js` and ran this successfully.
The service was removed.

Looks like a workable approach.  Just need to check offline installation.

### Windows Events

It was easy to have the application interact with the windows event system.

![](./images/windows-events.png)


### Offline Install

I was able to install node-windows locally and preform all the activities.  
So the next step is to zip up this project, ship to another server and check a service can be created.




