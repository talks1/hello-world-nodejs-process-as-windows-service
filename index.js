
    var EventLogger = require('node-windows').EventLogger;

function run(){
    console.log('.')

    var log = new EventLogger('ProductionDashboard');

    log.info('Basic information.');
    log.warn('Watch out!');
    log.error('Something went wrong.');

    setTimeout(run,10000)
}

run()